package book_service;

import com.lms.book_service.BookServiceApplication;
import com.lms.book_service.dtos.BookDTO;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static com.lms.book_service.utils.UtilBook.jsonString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookServiceApplication.class)
public class BookControllerTest {
    private static String BASE_URL = "/api/";
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void saveBookTest() throws Exception {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setName("Qari Muhammad Jamal" + new Date().getTime());
        bookDTO.setAuthorName("Qari Muhammad Jamal" + new Date().getTime());
        bookDTO.setCategoryId(1);

        mockMvc.perform(
                post(BASE_URL + "book")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(jsonString(bookDTO)))
                .andExpect(status().isCreated())
        ;
    }


}
