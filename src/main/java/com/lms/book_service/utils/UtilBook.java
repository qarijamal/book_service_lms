package com.lms.book_service.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UtilBook {
    public static String jsonString(final Object obj) {
        try {
            return new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}
