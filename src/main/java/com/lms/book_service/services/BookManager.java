package com.lms.book_service.services;

import com.lms.book_service.dtos.BookDTO;
import com.lms.book_service.exception.BookNotFoundException;

import java.util.List;

public interface BookManager {
    String saveBook(BookDTO bookDTO) throws BookNotFoundException;

    String updateBook(BookDTO bookDTO);

    String deleteBook(long id);

    BookDTO getBook(long id);

    List<BookDTO> getBooks();
}
