package com.lms.book_service.services;

import com.lms.book_service.dtos.BookDTO;
import com.lms.book_service.entities.Book;
import com.lms.book_service.exception.BookNotFoundException;
import com.lms.book_service.repos.BookRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BookService implements BookManager {

    @Autowired
    private BookRepo bookRepo;

    @Override
    public String saveBook(BookDTO bookDTO) throws BookNotFoundException {
        this.bookRepo.save(new ModelMapper().map(bookDTO, Book.class));
        return "";
    }

    @Override
    public String updateBook(BookDTO bookDTO) {
        Book book = this.bookRepo.getBookById(bookDTO.getId());
        if (book == null) {
            throw new BookNotFoundException(HttpStatus.NOT_FOUND.value(), "Book Not Found");
        }
        this.bookRepo.save(new ModelMapper().map(bookDTO, Book.class));
        return "";
    }

    @Override
    public String deleteBook(long id) {
        Book book = this.bookRepo.getBookById(id);
        if (book == null) {
            throw new BookNotFoundException(HttpStatus.NOT_FOUND.value(), "Book not found");
        }
        this.bookRepo.delete(book);
        return "";
    }

    @Override
    public BookDTO getBook(long id) throws BookNotFoundException {
        Book book = this.bookRepo.getBookById(id);
        if (book == null) {
            throw new BookNotFoundException(HttpStatus.NOT_FOUND.value(), "Book not found");
        }
        return new ModelMapper().map(book, BookDTO.class);
    }

    @Override
    public List<BookDTO> getBooks() {
        List<Book> books = this.bookRepo.findAll();
        if (books == null) {
            throw new BookNotFoundException(HttpStatus.NOT_FOUND.value(), "Books not available.");
        }
        List<BookDTO> bookDTOS = new ArrayList<>();
        for (Book book : books) {
            bookDTOS.add(new ModelMapper().map(book, BookDTO.class));
        }
        return bookDTOS;
    }
}
