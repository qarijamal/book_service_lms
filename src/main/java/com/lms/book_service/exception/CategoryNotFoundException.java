package com.lms.book_service.exception;

public class CategoryNotFoundException extends BookException {
    public CategoryNotFoundException(int code, String message) {
        super(code, message);
    }
}
