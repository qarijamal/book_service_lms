package com.lms.book_service.exception;

public class BookNotFoundException extends BookException {

    public BookNotFoundException(int code, String message) {
        super(code, message);
    }
}
