package com.lms.book_service.entities;

import javax.persistence.*;

@Entity
@Table
public class Book {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @Column(name = "author_name")
    private String authorName;
    @Column(name = "category_id")
    private long categoryId;

    public Book() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
}
