package com.lms.book_service.controllers;

import com.lms.book_service.dtos.BookDTO;
import com.lms.book_service.exception.BookNotFoundException;
import com.lms.book_service.exception.CategoryNotFoundException;
import com.lms.book_service.services.BookManager;
import com.lms.book_service.vos.BookVO;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class BookController {
    private static Logger logger = LoggerFactory.getLogger(BookController.class);
    @Autowired
    private BookManager bookManager;

    @PostMapping("book")
    public ResponseEntity<?> saveBook(@RequestBody BookVO bookVO) throws CategoryNotFoundException {
        logger.info("saveBook called.......");
        categoryException(bookVO.getCategoryId());
        this.bookManager.saveBook(new ModelMapper().map(bookVO, BookDTO.class));
        logger.info("saveBook ended.......");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("book/{id}")
    public ResponseEntity<?> getBook(@PathVariable("id") long id) {
        logger.info("getBook called.......");
        BookDTO bookDTO = this.bookManager.getBook(id);
        logger.info("getBook ended.......");
        return new ResponseEntity(bookDTO, null, HttpStatus.OK);
    }

    @PutMapping("book")
    public ResponseEntity<?> updateBook(@RequestBody BookVO bookVO) throws BookNotFoundException, CategoryNotFoundException {
        logger.info("updateBook called.......");
        if (bookVO.getId() == null || bookVO.getId() <= 0) {
            throw new BookNotFoundException(HttpStatus.NOT_FOUND.value(), "Book id required");
        }
        categoryException(bookVO.getCategoryId());
        this.bookManager.updateBook(new ModelMapper().map(bookVO, BookDTO.class));
        logger.info("updateBook ended.......");
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("book/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        logger.info("delete  called.......");
        this.categoryException(id);
        this.bookManager.deleteBook(id);
        logger.info("delete  ended.......");
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("book/all")
    public ResponseEntity<?> getBooks() {
        logger.info("getUsers called.......");
        return new ResponseEntity(this.bookManager.getBooks(), null, HttpStatus.OK);
    }

    private void categoryException(long categoryId) throws CategoryNotFoundException {
        if (categoryId <= 0) {
            throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(), "Required category id.");
        }
    }
}
