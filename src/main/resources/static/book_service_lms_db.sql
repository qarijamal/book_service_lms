/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.25-log : Database - book_service_lms_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`book_service_lms_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `book_service_lms_db`;

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(255) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `book` */

insert  into `book`(`id`,`author_name`,`category_id`,`name`) values (6,'Qari Muhammad Jamal1549561064931',1,'Qari Muhammad Jamal1549561064931'),(11,'Oracle updated',1,'Java updated'),(3,'Oracle updated',1,'Java updated'),(4,'Oracle updated',1,'Java updated'),(5,'Oracle updated',1,'Java updated'),(7,'Qari Muhammad Jamal1549561113694',1,'Qari Muhammad Jamal1549561113694'),(8,'Qari Muhammad Jamal1549561142642',1,'Qari Muhammad Jamal1549561142642'),(9,'Qari Muhammad Jamal1549561178960',1,'Qari Muhammad Jamal1549561178960'),(10,'Qari Muhammad Jamal1549561210079',1,'Qari Muhammad Jamal1549561210079'),(12,'Oracle updated',1,'Java updated'),(13,'Oracle updated',1,'Java updated'),(14,'Oracle updated',1,'Java updated'),(15,'Oracle updated',1,'Java updated'),(16,'Oracle updated',1,'Java updated');

/*Table structure for table `hibernate_sequence` */

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `hibernate_sequence` */

insert  into `hibernate_sequence`(`next_val`) values (17);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
